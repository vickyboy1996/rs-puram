import { Suspense, lazy } from 'react';
import { Navigate, useRoutes, useLocation } from 'react-router-dom';
// layouts
import DashboardLayout from '../layouts/dashboard';
import LogoOnlyLayout from '../layouts/LogoOnlyLayout';
// components
import LoadingScreen from '../components/LoadingScreen';

// ----------------------------------------------------------------------

const Loadable = (Component) => (props) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { pathname } = useLocation();

  return (
    <Suspense fallback={<LoadingScreen isDashboard={pathname.includes('/dashboard')} />}>
      <Component {...props} />
    </Suspense>
  );
};

export default function Router() {
  return useRoutes([
    {
      path: '/',
      element: <DashboardLayout />,
      children: [
        { element: <Navigate to="/dashboard/ten" replace />, index: true },
        { path: '/dashboard', element: <Navigate to="/dashboard/ten" replace />, index: true },
        { path: '/dashboard/one', element: <PageOne /> },
        { path: '/dashboard/two', element: <PageTwo /> },
        { path: '/dashboard/five', element: <PageFive /> },
        { path: '/dashboard/three', element: <PageThree /> },
        { path: '/dashboard/four', element: <PageFour /> },
        { path: '/dashboard/six', element: <PageSix /> },
        { path: '/dashboard/seven', element: <PageSeven /> },
        { path: '/dashboard/eight', element: <PageEight /> },
        { path: '/dashboard/nine', element: <PageNine /> },
        { path: '/dashboard/ten', element: <PageTen /> }


      ],
    },
    {
      path: '*',
      element: <LogoOnlyLayout />,
      children: [
        { path: '404', element: <NotFound /> },
        { path: '*', element: <Navigate to="/404" replace /> },
      ],
    },
    { path: '*', element: <Navigate to="/404" replace /> },
  ]);
}

// Dashboard
const PageOne = Loadable(lazy(() => import('../pages/Pageincome2022')));
const PageTwo = Loadable(lazy(() => import('../pages/PagePayment')));
const PageThree = Loadable(lazy(() => import('../pages/Pageexpense2022')));
const NotFound = Loadable(lazy(() => import('../pages/Page404')));
const PageFour = Loadable(lazy(() => import('../pages/Pageincome2023')));
const PageFive = Loadable(lazy(() => import('../pages/Pageover2022')));
const PageSix = Loadable(lazy(() => import('../pages/Pageexpenses2023')));
const PageSeven = Loadable(lazy(() => import('../pages/Pageover2023')));
const PageEight = Loadable(lazy(() => import('../pages/Pageincome2024')));
const PageNine = Loadable(lazy(() => import('../pages/Pageexpenses2024')));
const PageTen = Loadable(lazy(() => import('../pages/Pageover2024')));


