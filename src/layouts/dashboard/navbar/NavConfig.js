// components
import SvgIconStyle from '../../../components/SvgIconStyle';

// ----------------------------------------------------------------------

const getIcon = (name) => <SvgIconStyle src={`/icons/${name}.svg`} sx={{ width: 1, height: 1 }} />;

const ICONS = {
  user: getIcon('ic_user'),
  ecommerce: getIcon('ic_ecommerce'),
  analytics: getIcon('ic_analytics'),
  dashboard: getIcon('ic_dashboard'),
};

const sidebarConfig = [
  // GENERAL
  // ----------------------------------------------------------------------
  {
    subheader: 'Options',
    items2022: [
      { title: 'Income(வரவு)-2022', path: '/dashboard/one', icon: ICONS.dashboard },
      { title: 'Expenses(செலவு)-2022', path: '/dashboard/three', icon: ICONS.analytics },
      { title: '2022- Kovil kodai', path: '/dashboard/five', icon: ICONS.dashboard },
    ],
    items2023: [
      { title: 'Income(வரவு)-2023', path: '/dashboard/four', icon: ICONS.dashboard },
      { title: 'Expenses(செலவு)-2023', path: '/dashboard/six', icon: ICONS.analytics }, 
      { title: '2023- Kovil kodai', path: '/dashboard/seven', icon: ICONS.dashboard },
     
    ],
    items2024: [
      { title: 'Income(வரவு)-2024', path: '/dashboard/eight', icon: ICONS.dashboard },
      { title: 'Expenses(செலவு)-2024', path: '/dashboard/nine', icon: ICONS.analytics }, 
      { title: '2024- Kovil kodai', path: '/dashboard/ten', icon: ICONS.dashboard },
     
    ],
    itemspay: [
      { title: 'Future payment details', path: '/dashboard/two', icon: ICONS.ecommerce }
      
    ],
  }
];

export default sidebarConfig;
