import PropTypes from 'prop-types';
// @mui
import { styled } from '@mui/material/styles';
import { Box, Stack, AppBar, Toolbar } from '@mui/material';
// hooks
import useOffSetTop from '../../../hooks/useOffSetTop';
import useResponsive from '../../../hooks/useResponsive';
// utils
import cssStyles from '../../../utils/cssStyles';
// config
import { HEADER, NAVBAR } from '../../../config';
// components
import Logo from '../../../components/Logo';
import Iconify from '../../../components/Iconify';
import { IconButtonAnimate } from '../../../components/animate';
//
import AccountPopover from './AccountPopover';
import AppData from '../../../pages/data2024.json';

// ----------------------------------------------------------------------


const RootStyle = styled(AppBar, {
  shouldForwardProp: (prop) => prop !== 'isCollapse' && prop !== 'isOffset' && prop !== 'verticalLayout',
})(({ isCollapse, isOffset, verticalLayout, theme }) => ({
  ...cssStyles(theme).bgBlur(),
  boxShadow: 'none',
  height: HEADER.MOBILE_HEIGHT,
  zIndex: theme.zIndex.appBar + 1,
  transition: theme.transitions.create(['width', 'height'], {
    duration: theme.transitions.duration.shorter,
  }),
  [theme.breakpoints.up('lg')]: {
    height: HEADER.DASHBOARD_DESKTOP_HEIGHT,
    width: `calc(100% - ${NAVBAR.DASHBOARD_WIDTH + 1}px)`,
    ...(isCollapse && {
      width: `calc(100% - ${NAVBAR.DASHBOARD_COLLAPSE_WIDTH}px)`,
    }),
    ...(isOffset && {
      height: HEADER.DASHBOARD_DESKTOP_OFFSET_HEIGHT,
    }),
    ...(verticalLayout && {
      width: '100%',
      height: HEADER.DASHBOARD_DESKTOP_OFFSET_HEIGHT,
      backgroundColor: theme.palette.background.default,
    }),
  },
}));


// ----------------------------------------------------------------------

DashboardHeader.propTypes = {
  onOpenSidebar: PropTypes.func,
  isCollapse: PropTypes.bool,
  verticalLayout: PropTypes.bool,
};


export default function DashboardHeader({ onOpenSidebar, isCollapse = false, verticalLayout = false }) {
  let total =0
  let confirmedamount = 0
  let exptotal =0
  const isOffset = useOffSetTop(HEADER.DASHBOARD_DESKTOP_HEIGHT) && !verticalLayout;
  const isDesktop = useResponsive('up', 'lg');
  const incomarray =AppData.collection
  /* eslint-disable-next-line no-plusplus */
  for (let index = 0; index < incomarray.length; index++) {
    if (incomarray[index].status==="Paid")
    total += +incomarray[index].amount
  }
  /* eslint-disable-next-line no-plusplus */
  for (let index = 0; index < incomarray.length; index++) {
    if ( incomarray[index].status==="Unpaid")
    confirmedamount += +incomarray[index].amount
  }


  const exarray =AppData.expenseData
  /* eslint-disable-next-line no-plusplus */
  for (let index = 0; index < exarray.length; index++) {
    exptotal += +exarray[index].totalAmount
  }

  const inhand = total-exptotal
  
  return (
    
    <RootStyle
      style={{
        borderBottomWidth: 1,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderTopWidth: 0,
        borderStyle: 'solid',
        borderColor: 'green',
      }}
      isCollapse={isCollapse}
      isOffset={isOffset}
      verticalLayout={verticalLayout}
    >
      <Toolbar
      
        sx={{
          minHeight: '100% !important',
          px: { lg: 5 },
        }}
      >

        {isDesktop && verticalLayout && <Logo sx={{ mr: 2.5 }} />}

        {!isDesktop && (
          <IconButtonAnimate onClick={onOpenSidebar} sx={{ mr: 1, color: 'text.primary' }}>
            <Iconify icon="eva:menu-2-fill" color='#FB01EF' />
          </IconButtonAnimate>
        )}

        <Box sx={{ flexGrow: 1 }} />

        <Stack direction="row" alignItems="center" spacing={{ xs: 0.5, sm: 1.5 }}>
          <Stack direction="column">
            <Stack direction="row" mr={1}>
              <Box color={'#EF33CB'}fontWeight={'900'} >Details of 2024</Box>
            </Stack>
            <Stack direction="row" mr={2}>
              <Box color={'black'}>கையிருப்பு </Box>
              <Box color={'green'} ml={1}>
              ₹{inhand}
              </Box>
            </Stack>
            <Stack direction="row" mr={2}>
              <Box color={'black'}>மொத்த செலவு </Box>
              <Box color={'red'} ml={1}>
              ₹{exptotal}
              </Box>
            </Stack>
            <Stack direction="row" mr={3}>
              <Box color={'black'}>மொத்த வரவு </Box>
              <Box color={'green'} ml={1}>
              ₹{total}
              </Box>
            </Stack>
             {/* <Stack direction="row" mr={4}>
              <Box color={'black'}>Confirmed amount </Box>
              <Box color={'green'} ml={1}>
              ₹{confirmedamount}
              </Box>
            </Stack> */}
          </Stack>
          <AccountPopover />
        </Stack>
      </Toolbar>
    </RootStyle>
  );
}
