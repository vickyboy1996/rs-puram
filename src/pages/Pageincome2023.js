/* eslint-disable react/jsx-key */
// @mui
import { Box, Stack, Container, Typography, Card, CardContent, TextField, Avatar } from '@mui/material';
// hooks
import { useState } from 'react';
 
import useSettings from '../hooks/useSettings';
// components
import Page from '../components/Page';
import AppData from './data2023.json';
// ----------------------------------------------------------------------

export default function PageOne() {
  const DataLoad = AppData;
  const { themeStretch } = useSettings();
  const [filterText, setFilteredText] = useState('');

  let total =0
  const incomarray =AppData.collection
  /* eslint-disable-next-line no-plusplus */
  for (let index = 0; index < incomarray.length; index++) {
    if (incomarray[index].status==="Paid" || incomarray[index].status==="paid")
    total += +incomarray[index].amount
  }

  const handleChange = (event) => {
    // Access input value
    const query = event.target.value;
    // Trigger render with updated values
    setFilteredText(query);
  };

  const newFilter = DataLoad.collection
    .filter(({ name }) => name.toLowerCase().indexOf(filterText.toLowerCase()) >= 0)
    .sort((a, b) => b.amount - a.amount)
    .map((item) => (
      <Card sx={{ width: '100%' }}>
            <CardContent>
              <Typography textTransform={'uppercase'} fontWeight={'bold'}>
                {`${item.name}`}
              </Typography>
             { item.sonof && <Typography>{`S/O. ${item.sonof}`}</Typography>}
              <Stack flex={'1'} justifyContent="space-between" flexDirection={'row'}>
                <Box>
                  <Avatar src={`../profile_images/${item.image}.jpg`} alt = {`${item.name}`[0]} sx={{ width: 86, height: 86 }} variant="square"> {`${item.name}`[0]} </Avatar> 
                </Box>
                <Box>
                  <Typography>Amount</Typography>
                  <Typography>Status</Typography>
                  <Typography>Mobile</Typography>
                  <a href={AppData.whatsapp +item.mobile}>
                  <img src={`../profile_images/whatsapp_logo.jpg`} alt="HTML tutorial" width="25" height="25"/> 
                  </a>
                </Box>
                
                <Box marginX={2}>
                  <Typography fontWeight={'bold'}>₹{item.amount}</Typography>
                  
                  
                  <Typography fontWeight={'bold'} color={item.status === 'Paid' || item.status === 'Confirmed' ? 'green' : 'red'}  >
                    {item.status}
                  </Typography>
                  <a href={AppData.calling +item.mobile}>{item.mobile}</a>

                </Box>
              </Stack>
            </CardContent>
          </Card>
    ));

  return (
    <Page title="Page One">
      
      <Container maxWidth={themeStretch ? false : 'xl'}>
        <TextField
          id="search"
          type="search"
          label="Search by name"
          value={filterText}
          onChange={handleChange}
          sx={{ width: '100%' }}
        />
        <Card>
          <CardContent>
          <Typography textTransform={'uppercase'} fontWeight={'bold'} color={'green'}>Total Income ₹{total}</Typography>
          </CardContent>
        </Card>
        {newFilter}
      </Container>
    </Page>
  );
}
