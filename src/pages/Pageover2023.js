// @mui

import React from "react";
import PropTypes from "prop-types";
import { Box, Stack, Container, Typography, Card, CardActions, ImageList,ImageListItem} from '@mui/material';
// hooks
import useSettings from '../hooks/useSettings';
// components
import Page from '../components/Page';
import Amman1 from '../2023images/AMMAN1.jpg';
import Amman2 from '../2023images/AMMAN2.jpg';
import SENKIDASUDALAI from '../2023images/SENKIDASUDALAI.jpg';
import SUDALAI from '../2023images/SUDALAI.jpg';

import AppData from './data2023.json';




// ----------------------------------------------------------------------

export default function PageFive() {
  const { themeStretch } = useSettings();

  const itemData = [
    {
      img: Amman1,
      title: 'AMMAN1',
    },
    {
      img: Amman2,
      title: 'AMMAN2',
    },
    {
      img: SENKIDASUDALAI,
      title: 'SENKIDASUDALAI',
    },
    {
      img: SUDALAI,
      title: 'SUDALAI',
    }
  ];

  let total =0
  let exptotal =0
  const incomarray =AppData.collection
  /* eslint-disable-next-line no-plusplus */
  for (let index = 0; index < incomarray.length; index++) {
    if (incomarray[index].status==="Paid")
    total += +incomarray[index].amount
  }


  const exarray =AppData.expenseData
  /* eslint-disable-next-line no-plusplus */
  for (let index = 0; index < exarray.length; index++) {
    exptotal += +exarray[index].totalAmount
  }

  const inhand = total-exptotal

  const YoutubeEmbed = ({ embedId }) => (
  <div className="video-responsive">
    <iframe
      width="412"
      height="350"
      src={`https://www.youtube.com/embed/${embedId}`}
      frameBorder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen
      title="Embedded youtube"
    />
  </div>
);

YoutubeEmbed.propTypes = {
  embedId: PropTypes.string.isRequired
};


  return (
    <Page title="Page Five">
      <Container maxWidth={themeStretch ? false : 'xl'}>
        <Card sx={{ width: '100%' }}>
          <CardActions>
          <Stack m={2} flex={'1'} flexDirection={'column'}>
            <Box>
            <Typography color={'green'} fontWeight={'bold'}> மொத்த வரவு (2023) - ₹{total}</Typography>
            </Box>
          </Stack>
          </CardActions>
        </Card>
        <Card sx={{ width: '100%' }}>
          <CardActions>
          <Stack m={2} flex={'1'} flexDirection={'column'}>
            <Box>
            <Typography color={'red'} fontWeight={'bold'}> மொத்த செலவுகள் (2023) - ₹{exptotal}</Typography>
            </Box>
            </Stack>
          </CardActions>
        </Card>
        <Card sx={{ width: '100%' }}>
          <CardActions>
          <Stack m={2} flex={'1'} flexDirection={'column'}>
            <Box>
            <Typography color={'blue'} fontWeight={'bold'}> கையிருப்பு (2023) - ₹{inhand}</Typography>
            </Box>
            </Stack>
          </CardActions>
        </Card>
      </Container>
      <ImageList >
          {itemData.map((item) => (
            <ImageListItem key={item.img}>
              <img
                src={`${item.img}?w=164&h=164&fit=crop&auto=format`}
                srcSet={`${item.img}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                alt={item.title}
                loading="lazy"
              />
            </ImageListItem>
            )
          )
          }
      </ImageList>
      <YoutubeEmbed embedId="WYbaPyBUHiE?&autoplay=1" />
    </Page>
  );
}
