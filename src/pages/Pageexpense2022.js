/* eslint-disable react/jsx-key */
// @mui
import { Box, Stack, Container, Typography, Card, CardContent } from '@mui/material';
// hooks
import { useLayoutEffect, useState } from 'react';
import useSettings from '../hooks/useSettings';

// components
import Page from '../components/Page';
import AppData from './data.json';
// ----------------------------------------------------------------------

export default function PageThree() {
  const { themeStretch } = useSettings();
  const [data, setData] = useState([]);
  useLayoutEffect(() => {
    const sortData = AppData.expenseData.sort((a, b) => {
      const nameA = a.name.toLowerCase();
      const nameB = b.name.toLowerCase();
      if (nameA < nameB)
        // sort string ascending
        return -1;
      if (nameA > nameB) return 1;
      return 0; // default return value (no sorting)
    });
    setData(sortData);
  }, []);
  return (
    <Page title="Page Three">
      <Container>
        <Card>
          <CardContent>
          <Typography textTransform={'uppercase'} fontWeight={'bold'} color={'red'}>Total Expenses ₹78,775</Typography>
          </CardContent>
        </Card>
      </Container>
      <Container maxWidth={themeStretch ? false : 'xl'}>
        {data.map((item, i) => (
          <Card sx={{ width: '100%', mt: i === 0 ? 0 : 2 }}>
            <CardContent>
              <Typography textTransform={'uppercase'} fontWeight={'bold'}>
              {`${item.name}`}
              </Typography>
              {item.sonof && <Typography>{`S/O. ${item.sonof}`}</Typography>}
              <Stack flex={'1'} justifyContent="space-between" flexDirection={'row'}>
                <Box>
                  <Typography>Total Amount</Typography>
                  {item.advance && <Typography>Advance Paid</Typography>}
                </Box>
                <Box marginX={2}>
                  <Typography fontWeight={'bold'} color={'red'}>
                  ₹{item.totalAmount}
                  </Typography>
                  {item.advance && (
                    <Typography fontWeight={'bold'} color={'black'}>
                      {item.advanceAmount}
                    </Typography>
                  )}
                </Box>
              </Stack>
              <Typography>{item.note}</Typography>
            </CardContent>
          </Card>
        ))}
      </Container>
    </Page>
  );
}
