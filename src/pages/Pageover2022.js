// @mui

import React from "react";
import PropTypes from "prop-types";
import { Box, Stack, Container, Typography, Card, CardActions, ImageList, ImageListItem} from '@mui/material';
// hooks
import useSettings from '../hooks/useSettings';
// components
import Page from '../components/Page';
import Amman1 from './muthu_mariamman.jpg';
import Amman2 from './uchinimahali_amman.jpg';



// ----------------------------------------------------------------------

export default function PageFive() {
  const { themeStretch } = useSettings();

  const itemData = [
    {
      img: Amman1,
      title: 'AMMAN1',
    },
    {
      img: Amman2,
      title: 'AMMAN2',
    }
  ];

  const YoutubeEmbed = ({ embedId }) => (
  <div className="video-responsive">
    <iframe
      width="412"
      height="350"
      src={`https://www.youtube.com/embed/${embedId}`}
      frameBorder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen
      title="Embedded youtube"
    />
  </div>
);

YoutubeEmbed.propTypes = {
  embedId: PropTypes.string.isRequired
};


  return (
    <Page title="Page Five">
      <Container maxWidth={themeStretch ? false : 'xl'}>
        <Card sx={{ width: '100%' }}>
          <CardActions>
          <Stack m={2} flex={'1'} flexDirection={'column'}>
            <Box>
            <Typography color={'green'} fontWeight={'bold'}> மொத்த வரவு (2022) - ₹79,300</Typography>
            </Box>
          </Stack>
          </CardActions>
        </Card>
        <Card sx={{ width: '100%' }}>
          <CardActions>
          <Stack m={2} flex={'1'} flexDirection={'column'}>
            <Box>
            <Typography color={'red'} fontWeight={'bold'}> மொத்த செலவுகள் (2022) - ₹78,775</Typography>
            </Box>
            </Stack>
          </CardActions>
        </Card>
        <Card sx={{ width: '100%' }}>
          <CardActions>
          <Stack m={2} flex={'1'} flexDirection={'column'}>
            <Box>
            <Typography color={'blue'} fontWeight={'bold'}> கையிருப்பு (2022) - ₹525</Typography>
            </Box>
            </Stack>
          </CardActions>
        </Card>
      </Container>
      <ImageList >
          {itemData.map((item) => (
            <ImageListItem key={item.img}>
              <img
                src={`${item.img}?w=164&h=164&fit=crop&auto=format`}
                srcSet={`${item.img}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                alt={item.title}
                loading="lazy"
              />
            </ImageListItem>
            )
          )
          }
      </ImageList>
      <YoutubeEmbed embedId="FZIbr-_VFKU?&autoplay=1" />
      <YoutubeEmbed embedId="3BhO7ZcluuQ" />
       <YoutubeEmbed embedId="MdBKhcqNVYs" />
       <YoutubeEmbed embedId="45rZvBZnLnk" />
    </Page>
  );
}
