// @mui


import { Box, Stack, Container, Typography, Card, CardHeader, CardContent, Divider, CardActions, Link } from '@mui/material';
// hooks
import useSettings from '../hooks/useSettings';
// components
import Page from '../components/Page';



// ----------------------------------------------------------------------

export default function PageTwo() {
  const { themeStretch } = useSettings();

  return (
    <Page title="Page Two">
      <Container maxWidth={themeStretch ? false : 'xl'}>
        <Card sx={{ width: '100%' }}>
          <CardHeader title="Digital Payment" />
          <CardContent>
            <Stack flex={'1'} flexDirection={'row'}>
              <Box>
              <Typography variant="subtitle2" noWrap>
            Account Number
          </Typography>
                <Typography>IFSC Code</Typography>
                <Typography>Bank Name</Typography>
                <Typography>Account Holder</Typography>
                
              </Box>
              <Box  marginX={2}>
                <Typography>007701559898</Typography>
                <Typography>ICIC0000077</Typography>
                <Typography>ICICI BANK</Typography>
                <Typography>LOGARAJA SELVAKUMARAN M</Typography>
                

              </Box>
            </Stack>
            <Divider />
            <Typography mt={2} fontWeight={'bold'}>Gpay or Phonepe</Typography>
            <Stack flex={'1'} flexDirection={'row'}>
              <Box>
                <Typography>Phone Number</Typography>
              </Box>
              <Box  marginX={2}>
                <Typography>7402458544</Typography>
              </Box>
            </Stack>
            <Divider />
            <Typography mt={2} fontWeight={'bold'}>UPI ID</Typography>
            <Stack flex={'1'} flexDirection={'row'}>
              <Box>
                <Typography>ID</Typography>
              </Box>
              <Box  marginX={2}>
              <Link href="#">rkumaran19963@ybl</Link>
              </Box>
            </Stack>
            <Divider />
            <Divider />
            <Typography mt={2} fontWeight={'bold'}>Payment</Typography>
            <Stack flex={'1'} flexDirection={'row'}>
              <Box  marginX={2}>
              <a href="phonepe://pay?pa=rkumaran19963@ybl&pn=Logaraja%20Selvakumaran%20M&tn=RSPuram%20Donation&cu=INR" > <img src = {`../profile_images/Phonepe.jpg`} alt="Phonepe" height="55" size="40"/> </a>
              </Box>
            </Stack>
            <Divider />
            </CardContent>
          
          <CardActions>
          <Stack m={2} flex={'1'} flexDirection={'column'}>
            <Box>
            <Typography color={'red'} fontWeight={'bold'}>Note:</Typography>
            <Typography>Update Raja kumaran after sending money. Any clarification feel free to call.</Typography>
            </Box>
            <Box>
            <Typography color={'red'} fontWeight={'bold'}>Mobile:</Typography>
            <Typography>+91-7904659325</Typography>
            <a href={"https://wa.me/+917402458544?text=Hi%20Raja%20Please%20help%20me%20regarding%20RSPuram%20Donation" }><img src = {`../profile_images/Whatsapp_logo.jpg`} alt="whatsapp" height="55" size="40" /> </a> 
            </Box>
            </Stack>
          </CardActions>
        </Card>
      </Container>
    </Page>
  );
}
