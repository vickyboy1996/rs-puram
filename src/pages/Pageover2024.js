// @mui

import React from 'react';
import PropTypes from 'prop-types';
import { Box, Stack, Container, Typography, Card, CardActions, ImageList, ImageListItem, Button } from '@mui/material';
// hooks
import useSettings from '../hooks/useSettings';
// components
import Page from '../components/Page';
import Amman1 from '../2024images/AMMAN1.jpeg';
import Amman2 from '../2024images/AMMAN2.jpeg';
import SUDALAI from '../2024images/SUDALAI.jpeg';
import PECHI from '../2024images/PECHI.jpeg';

import AppData from './data2024.json';

// ----------------------------------------------------------------------

export default function PageFive() {
  const { themeStretch } = useSettings();

  const itemData = [
    {
      img: Amman1,
      title: 'AMMAN1',
    },
    {
      img: Amman2,
      title: 'AMMAN2',
    },
    {
      img: SUDALAI,
      title: 'SUDALAI',
    },
    {
      img: PECHI,
      title: 'PECHI',
    },
  ];

  let total = 0;
  let exptotal = 0;
  const incomarray = AppData.collection;
  /* eslint-disable-next-line no-plusplus */
  for (let index = 0; index < incomarray.length; index++) {
    if (incomarray[index].status === 'Paid') total += +incomarray[index].amount;
  }

  const exarray = AppData.expenseData;
  /* eslint-disable-next-line no-plusplus */
  for (let index = 0; index < exarray.length; index++) {
    exptotal += +exarray[index].totalAmount;
  }

  const inhand = total - exptotal;

  const YoutubeEmbed = ({ embedId }) => (
    <div className="video-responsive">
      <iframe
        width="412"
        height="350"
        src={`https://www.youtube.com/embed/${embedId}`}
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
        title="Embedded youtube"
      />
    </div>
  );

  YoutubeEmbed.propTypes = {
    embedId: PropTypes.string.isRequired,
  };

  return (
    <Page title="Page Five">
      <Container maxWidth={themeStretch ? false : 'xl'}>
        <Card sx={{ width: '100%' }}>
          <CardActions>
            <Stack m={2} flex={'1'} flexDirection={'column'}>
              <Box>
                <Typography color={'red'} fontWeight={'bold'}>
                  {' '}
                  நன்கொடை வழங்கிய அனைத்து நல்ல உள்ளங்களுக்கும் நன்றி. 🙏🙏🙏
                </Typography>
              </Box>
            </Stack>
          </CardActions>
        </Card>
        <Card sx={{ width: '100%' }}>
          <CardActions>
            <Stack m={2} flex={'1'} flexDirection={'column'}>
              <Box>
                <Typography color={'blue'} fontWeight={'bold'}>
                  {' '}
                  மொத்த வரவு (2024) - ₹{total}
                </Typography>
                <Button variant="contained" href="/dashboard/eight" color="success">
                  முழு வரவு விவரம் தெரிய Click செய்யவும்
                </Button>
              </Box>
            </Stack>
          </CardActions>
        </Card>
        <Card sx={{ width: '100%' }}>
          <CardActions>
            <Stack m={2} flex={'1'} flexDirection={'column'}>
              <Box>
                <Typography color={'blue'} fontWeight={'bold'}>
                  {' '}
                  மொத்த செலவு (2024) - ₹{exptotal}
                </Typography>
                <Button variant="contained" href="/dashboard/nine" color="error">
                  முழு செலவு விவரம் தெரிய Click செய்யவும்
                </Button>
              </Box>
            </Stack>
          </CardActions>
        </Card>
        <Card sx={{ width: '100%' }}>
          <CardActions>
            <Stack m={2} flex={'1'} flexDirection={'column'}>
              <Box>
                <Typography color={'blue'} fontWeight={'bold'}>
                  {' '}
                  கையிருப்பு (2024) - ₹{inhand}
                </Typography>
              </Box>
            </Stack>
          </CardActions>
        </Card>
      </Container>
      <ImageList>
        {itemData.map((item) => (
          <ImageListItem key={item.img}>
            <img src={`${item.img}`} srcSet={`${item.img}`} alt={item.title} loading="lazy" />
          </ImageListItem>
        ))}
      </ImageList>
      <YoutubeEmbed embedId="KZvpo0CMNV4?&autoplay=1" />
      <YoutubeEmbed embedId="cd5gSdfTzlM" />
      <YoutubeEmbed embedId="pTNe09fkoeQ" />
      <YoutubeEmbed embedId="-eBFKiTqRhs" />
      <YoutubeEmbed embedId="mqUcEZx9Q8w" />
      <YoutubeEmbed embedId="DRzT_9FltY4" />
      <YoutubeEmbed embedId="uVVKqYvrjIc" />
      <YoutubeEmbed embedId="wzT05kwkyA8" />
    </Page>
  );
}
